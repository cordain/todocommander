use cucumber::{given,when,then,World,gherkin::Step};
use todocommander::listmanager::{TodoListManager, ToDoTask};
use todocommander::transactions::{Transactions, TransactionResponse};

use std::cmp::Ordering;
use std::fs::OpenOptions;
use std::io::prelude::*;

#[derive(World,Debug)]
struct WorldTodoListManager{
    manager: TodoListManager,
    error: Result<TransactionResponse,String>
}

impl Default for WorldTodoListManager{
    fn default() -> Self {
        Self { manager: Default::default(), error: Ok(TransactionResponse::Empty) }
    }
}

#[given("task list exists in memory")]
fn initialize_task_list_file(_: &mut WorldTodoListManager){
    std::fs::remove_file("TaskListTestFile").unwrap_or_default();
    OpenOptions::new()
        .create_new(true)
        .write(true)
        .append(true)
        .open("TaskListTestFile")
        .map(|_|())
        .unwrap()
    ;
}
#[given("task list already have following tasks by name")]
fn fill_task_list(_: &mut WorldTodoListManager, step: &Step) {
    let mut file = OpenOptions::new()
        .write(true)
        .open("TaskListTestFile")
        .unwrap()
    ;
    let mut stringbuf = String::default();
    if let Some(tasks) = step.table.as_ref(){
        for row in tasks.rows.iter(){
            stringbuf.push_str(row[0].as_str());
            stringbuf.push('\n');
        }
        stringbuf.pop().unwrap();
    }
    file.write_all(stringbuf.as_bytes()).unwrap();
}

#[when("user adds task with following information")]
fn user_adds_task_with_parameters(world: &mut WorldTodoListManager, step: &Step) {
    let mut task = String::from("");
    if let Some(parameters) = step.table.as_ref(){
        for row in parameters.rows.iter(){
            let param = &row[0];
            match param.as_str(){
                "Name" => task = row[1].clone(),
                _ => unreachable!()
            }
        }
    }
    world.manager.new_task(ToDoTask::from_label(task));
}
#[when("user deletes task with name")]
fn user_deletes_task_by_name(world: &mut WorldTodoListManager, step: &Step) {
    let mut file = OpenOptions::new()
        .read(true)
        .open("TaskListTestFile")
        .unwrap()
    ;
    world.manager = TodoListManager::from_file(&mut file).unwrap();
    let taskname = step.table.as_ref().unwrap().rows[0][0].clone();
    let action_result = world.manager.delete_by_name(taskname).map(|_| TransactionResponse::Empty).map_err(|err|err.to_string());
    world.error = action_result;
}
#[when("user closes program")]
fn user_terminates_program(world: &mut WorldTodoListManager){
    let mut file = OpenOptions::new()
        .write(true)
        .truncate(true)
        .open("TaskListTestFile")
        .unwrap()
    ;
    let action_result = world.manager.close(&mut file).map(|_| TransactionResponse::Empty).map_err(|err|err.to_string());
    world.error = action_result;
}
#[when("user renames task with name to different name")]
fn user_renames_task(world: &mut WorldTodoListManager, step: &Step){
    let mut file = OpenOptions::new()
        .read(true)
        .open("TaskListTestFile")
        .unwrap()
    ;
    world.manager = TodoListManager::from_file(&mut file).unwrap();
    let (name_from, name_to): (String,String) = {
        let row = &step.table
            .clone()
            .unwrap()
            .rows[0]
        ;
        (row[0].to_owned(),row[1].to_owned())
    };
    let action_result = world.manager.rename_task(name_from,name_to).map(|_| TransactionResponse::Empty).map_err(|err|err.to_string());
    world.error = action_result;
}

#[when(regex = r"^user wants to (\S*) the ((?:\S* )*)into ((?:\S* )*)using command line$")]
fn execute_command_with_basic_parameters(
    world: &mut WorldTodoListManager,
    command: String,
    target: String,
    resolution: String
){
    let arguments = {
        let mut arguments = Vec::<String>::default();
        if !command.is_empty(){
            arguments.push(command.clone())
        }
        let target = target.trim_end().to_string();
        if !target.is_empty(){
            arguments.push(target)
        }
        if command.contains("rename"){
            let resolution = resolution.trim_end().to_string();
            if !resolution.is_empty(){
                arguments.push(resolution);
            }
        }
        arguments
    };

    let mut file = OpenOptions::new()
        .read(true)
        .open("TaskListTestFile")
        .unwrap()
    ;
    world.manager = TodoListManager::from_file(&mut file).unwrap();
    let transaction = Transactions::from_parameters(arguments);
    let action_result = world.manager.execute(transaction).map_err(|err|String::from(err));
    world.error = action_result;
}
#[when("user wants to print tasks")]
fn print_tasks(world: &mut WorldTodoListManager){
    let arguments = vec!["print".to_string()];
    let mut file = OpenOptions::new()
        .read(true)
        .open("TaskListTestFile")
        .unwrap()
    ;
    world.manager = TodoListManager::from_file(&mut file).unwrap();
    let transaction = Transactions::from_parameters(arguments);
    let action_result = world.manager.execute(transaction).map_err(|err|String::from(err));
    world.error = action_result;
}

#[then("following task is saved in memory")]
fn assert_task_is_in_task_database(world: &mut WorldTodoListManager, step: &Step){
    let mut taskname = String::default();
    if let Some(parameters) = step.table.as_ref(){
        for row in parameters.rows.iter(){
            let param = &row[0];
            match param.as_str(){
                "Name" => taskname = row[1].clone(),
                _ => unreachable!()
            }
        }
    }
    let tasks: Vec<String> = {
        let mut taskstr = String::default();

        let _ = OpenOptions::new()
            .read(true)
            .open("TaskListTestFile")
            .unwrap()
            .read_to_string(&mut taskstr)
            .unwrap()
        ;
        taskstr
            .replace("\r", "")
            .split("\n")
            .map(|task| String::from(task))
            .collect()
    };
    //assert_eq!(world.error,Ok(TransactionResponse::Empty));//Sanity check
    assert!(tasks.iter().find(|task| task.contains(&taskname)).is_some())
}
#[then("following task is no longer in memory")]
fn assert_task_not_present_in_memory(world: &mut WorldTodoListManager, step: &Step){
    let mut file = OpenOptions::new()
        .read(true)
        .open("TaskListTestFile")
        .unwrap()
    ;
    let mut stringbuf = String::default();
    file.read_to_string(&mut stringbuf).unwrap();
    let taskname = step.table.as_ref().unwrap().rows[0][0].clone();
    assert_eq!(world.error,Ok(TransactionResponse::Empty));//Sanity check
    assert_eq!(stringbuf.contains(&format!("\n{}",taskname)),false);
}
#[then(regex = r"^list has items: (.+)$")]
fn assert_updated_items(world: &mut WorldTodoListManager, tasks: String){
    let tasklist = {
        let mut taskstr = String::default();
        let _ = OpenOptions::new()
            .read(true)
            .open("TaskListTestFile")
            .unwrap()
            .read_to_string(&mut taskstr)
            .unwrap()
        ;
        taskstr
            .split("\n")
            .map(|task| ToDoTask::from_label(task.to_string()))
            .collect::<Vec<ToDoTask>>()

    };
    let tasklist_expected = tasks
        .split(", ")
        .map(|task| ToDoTask::from_label(task.to_string()))
        .collect::<Vec<ToDoTask>>()
    ;
    assert_eq!(world.error,Ok(TransactionResponse::Empty));//Sanity check
    assert_eq!(tasklist_expected.iter().cmp(tasklist.iter()),Ordering::Equal);
}
#[then(regex = r"((?:\S+ )+)prints")]
fn assert_error_printing(world: &mut WorldTodoListManager, error_message: String){
    assert_eq!(world.error,Err(error_message.trim_end().to_string()));
}
#[then("following tasks are printed")]
fn assert_printed_errors(world: &mut WorldTodoListManager, step: &Step){
    let list_expected = step.table.as_ref().unwrap().rows
        .iter()
        .map(|row| row[0].clone())
        .collect::<Vec<String>>()
    ;
    let response = world.error.as_ref().ok().unwrap();
    //Extract only valid response type
    if let TransactionResponse::PrintList { list } = response {
        assert_eq!(list_expected.iter().cmp(list.iter()),Ordering::Equal);
    }
    else {
        panic!()
    }
}

fn main(){
    futures::executor::block_on(WorldTodoListManager::run("tests/features"));
}