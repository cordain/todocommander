use std::cmp::Ordering;
use std::fs::OpenOptions;
use std::io::prelude::*;
use rand::distributions::{Alphanumeric, DistString};

use todocommander::{listmanager::{ToDoTask,TodoListManager}, transactions::{Transactions, TransactionResponse}};
use rstest::rstest;

#[test]
fn when_task_with_label_is_provided_then_it_is_stored_in_runtime(){
    let taskname = String::from("Test Task");
    let task = ToDoTask::from_label(taskname.clone());
    let mut manager = TodoListManager::default();

    manager.new_task(task);
    
    assert!(manager.get_tasks().iter().find(|task| task.get_name().contains(&taskname)).is_some())
}

#[rstest]
#[case(vec!["Test Task"])]
#[case(vec!["Test Task", "Move out trash", "Implement this feature"])]
#[case(vec![])]
fn given_task_list_was_filled_in_runtime_when_manager_closes_then_tasks_are_saved_to_runtime(
    #[case] task_saved: Vec<&str>
){
    let path: String = Alphanumeric.sample_string(&mut rand::thread_rng(), 15);
    let tasks: Vec<ToDoTask> = task_saved
        .iter()
        .map(|label| ToDoTask::from_label(label.to_string()))
        .collect()
    ;
    let mut file = OpenOptions::new()
        .create(true)
        .append(true)
        .write(true)
        .read(true)
        .open(&path)
        .unwrap()
    ;
    let mut manager = TodoListManager::from_file(&mut file).unwrap();
    tasks
        .iter()
        .for_each(|task| manager.new_task(task.to_owned()))
    ;

    manager.close(&mut file).unwrap();

    let tasks_expected: Vec<ToDoTask> = {
        let mut stringbuff = String::default();
        OpenOptions::new()
            .read(true)
            .open(&path)
            .unwrap()
            .read_to_string(&mut stringbuff)
            .unwrap()
        ;
        if stringbuff.is_empty() {
            vec![]
        }
        else{
            stringbuff
                .replace("\r", "")
                .split("\n")
                .map(|taskname| ToDoTask::from_label(String::from(taskname)))
                .collect()
        }
        //return Vec<ToDoTask>
    };
    assert_eq!(tasks_expected.iter().cmp(tasks.iter()),Ordering::Equal);
    //cleanup
    std::fs::remove_file(&path).unwrap_or_default();
}

#[rstest]
#[case(vec!["Take a shower"],"Take a shower",Ok(()))]
#[case(vec![],"Take a shower",Err("Removing from empty list"))]
#[case(vec!["Take a shower", "Make a video", "Go to gym"],"Take a shower",Ok(()))]
#[case(vec!["Take a shower", "Make a video", "Go to gym"],"Do homework",Err("Removing task that does not exist"))]
fn when_user_deletes_task_then_task_is_not_in_memory(
    #[case] tasknames: Vec<&str>,
    #[case] delete_task: &str,
    #[case] delete_result_expected: Result<(),&str>
){
    let path: String = Alphanumeric.sample_string(&mut rand::thread_rng(), 15);
    let buff = tasknames.join("\n");
    let mut buff = buff.as_bytes();
    OpenOptions::new()
        .create(true)
        .append(true)
        .write(true)
        .open(&path)
        .unwrap()
        .write_all(&mut buff)
        .unwrap()
    ;
    let mut file = OpenOptions::new()
        .create(true)
        .append(true)
        .write(true)
        .read(true)
        .open(&path)
        .unwrap()
    ;
    let mut manager = TodoListManager::from_file(&mut file).unwrap();
    let delete_result = manager.delete_by_name(String::from(delete_task));

    assert_eq!(delete_result, delete_result_expected);
    //cleanup
    std::fs::remove_file(&path).unwrap_or_default();
}

#[rstest]
#[case(vec!["Take a shower"],"Take a shower","Take a bath",Ok(()))]
#[case(vec![],"Take a shower","Take a bath",Err("Renaming task that does not exist"))]
#[case(vec!["Take a shower", "Make a video", "Go to gym"],"Take a shower","Take a bath",Ok(()))]
#[case(vec!["Take a shower", "Make a video", "Go to gym"],"Do homework","Play with frients",Err("Renaming task that does not exist"))]
fn when_task_is_renamed_then_updated_task_is_saved(
    #[case] tasknames: Vec<&str>,
    #[case] taskrename_from: &str,
    #[case] taskrename_to: &str,
    #[case] rename_result_expected: Result<(),&str>
){
    let path: String = Alphanumeric.sample_string(&mut rand::thread_rng(), 15);
    let buff = tasknames.join("\n");
    let mut buff = buff.as_bytes();
    OpenOptions::new()
        .create(true)
        .append(true)
        .write(true)
        .open(&path)
        .unwrap()
        .write_all(&mut buff)
        .unwrap()
    ;
    let mut file = OpenOptions::new()
        .create(true)
        .append(true)
        .write(true)
        .read(true)
        .open(&path)
        .unwrap()
    ;
    let mut manager = TodoListManager::from_file(&mut file).unwrap();
    let rename_result = manager.rename_task(
        String::from(taskrename_from),
        String::from(taskrename_to)
    );

    assert_eq!(rename_result, rename_result_expected);
    //cleanup
    std::fs::remove_file(&path).unwrap_or_default();
}

#[rstest]
#[case(vec![],Err("No command provided"))]
#[case(vec!["add","Take a shower"],Ok(TransactionResponse::Empty))]
#[case(vec!["add","Go for lunch"],Err("Adding task that already exists"))]
#[case(vec!["add"],Err("No parameters provided for add command"))]
#[case(vec!["remove"],Err("No label provided to remove command"))]
#[case(vec!["remove","Make a date"],Ok(TransactionResponse::Empty))]
#[case(vec!["remove","Take a shower"],Err("Removing task that does not exist"))]
#[case(vec!["rename"],Err("No label provided to rename command"))]
#[case(vec!["rename","Take a shower"],Err("Renaming task that does not exist"))]
#[case(vec!["rename","Go for lunch"],Err("Task found, but new task name not provided"))]
#[case(vec!["rename","Go for lunch","Take a bath"],Ok(TransactionResponse::Empty))]
#[case(vec!["rename","Take a shower","Take a bath"],Err("Renaming task that does not exist"))]
#[case(vec!["print"],Ok(TransactionResponse::PrintList { list: vec!["Go for lunch".to_string(),"Make a date".to_string()]}))]
fn given_assumed_starting_list_when_user_provides_basic_arguments_then_transaction_is_executed_with_result(
    #[case] arguments: Vec<&str>,
    #[case] execution_result_expected: Result<TransactionResponse,&str>
){
    let mut manager = TodoListManager::default();
    manager.new_task(ToDoTask::from_label(String::from("Go for lunch")));
    manager.new_task(ToDoTask::from_label(String::from("Make a date")));
    let arguments_prepared: Vec<String> = arguments
        .iter()
        .map(|&arg| String::from(arg))
        .collect()
    ;

    let transaction = Transactions::from_parameters(arguments_prepared);
    let execution_result = manager.execute(transaction);

    assert_eq!(execution_result,execution_result_expected);
}

fn main(){
}