Feature: Command line interface
"""
User needs to somehow access his data right?
Command line parameters needs to be easy to type.
"""

  Background: 
    Given task list exists in memory
    And task list already have following tasks by name
      | Take a shower           |
      | Brush my teeth          |
      | Go for lunch with Daria |
      | Go to the shop          |
      | Feed the cat            |

  Scenario Outline: User wants to access the program
    When user wants to <action> the <task> into <place> using command line
    And user closes program
    Then list has items: <expected>

    Examples: 
      | action | task                    | place                    | expected                                                                                             |
      | add    | Take out trash          | list                     | Take a shower, Brush my teeth, Go for lunch with Daria, Go to the shop, Feed the cat, Take out trash |
      | remove | Go to the shop          | trash                    | Take a shower, Brush my teeth, Go for lunch with Daria, Feed the cat                                 |
      | rename | Go for lunch with Daria | Go for lunch with Sylvia | Take a shower, Brush my teeth, Go for lunch with Sylvia, Go to the shop, Feed the cat                |

  Scenario Outline: User badly accesses the program
  """
  Even veterans can make mistakes.
  """

    When user wants to <action> the <task> into <place> using command line
    Then <error> prints

    Examples: 
      | action | task           | place                  | error                                      |
      |        |                |                        | No command provided                        |
      | add    |                |                        | No parameters provided for add command     |
      | add    | Take a shower  |                        | Adding task that already exists            |
      | ad     | Take out trash |                        | Command not found                          |
      | remove |                |                        | No label provided to remove command        |
      | remove | Take out trash |                        | Removing task that does not exist          |
      | rename |                |                        | No label provided to rename command        |
      | rename | Take out trash | Take trash to yourself | Renaming task that does not exist          |
      | rename | Take out trash |                        | Renaming task that does not exist          |
      | rename | Take a shower  |                        | Task found, but new task name not provided |

  Scenario: User wants to read tasklist
  """
  What use is for todo task manager if tasks are hidden from the user?
  """

    When user wants to print tasks
    Then following tasks are printed
      | Take a shower           |
      | Brush my teeth          |
      | Go for lunch with Daria |
      | Go to the shop          |
      | Feed the cat            |
