Feature: Todo List Manager
"""
Basic functionality for tool is to add, edit and remove todo items. 
Featue shall handle situations typical for everyday usage
and some unexpected situations (at least from user perspective).
"""

  Background: 
    Given task list exists in memory
    And task list already have following tasks by name
      | Prepare breakfast |
      | Pet cat           |
      | Go to gym         |
      | Conquer the world |

  Scenario: User wants to save next task
    When user adds task with following information
      | Name | Take a shower |
    And user closes program
    Then following task is saved in memory
      | Name | Take a shower |

  Scenario: User wants to delete the task
    When user deletes task with name
      | Go to gym |
    And user closes program
    Then following task is no longer in memory
      | Go to gym |

  Scenario: User wants to rename the task
    When user renames task with name to different name
      | Go to gym | Go to bar |
    And user closes program
    Then following task is saved in memory
      | Name | Go to bar |
