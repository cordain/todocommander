Feature: Todo Commander
"""
This feature file will describe real life scenarios for project
"""
    @blackbox
    Scenario: Basic tasklist manipulation
        Given application is lunched for a first time
        When user does following actions
        * add task "Take a shower"
        * add task "Do shopping"
        * print tasks
        * remove task "Take a shower"
        * print tasks
        * add task "Make a date"
        * print tasks
        * rename task "Make a date" to "Make a cake"
        * print tasks
        Then following tasks are printed
        | Do shopping |
        | Make a cake |