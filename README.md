# TodoCommander

This program serves purpose of todo list manager.

# Usage
Todo ;)

# Development

Tool is written in Rust and developed via Acceptance Test-Driven Development methodology.
For acceptance tests Cucumber is used.
For integration and unit tests rstest is used.