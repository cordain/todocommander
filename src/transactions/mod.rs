#[cfg(test)]
mod tests;

#[derive(Debug,PartialEq)]
pub enum TransactionResponse{
    Empty,
    PrintList{list: Vec<String>}
}

#[derive(Default,Debug,PartialEq)]
pub enum InvalidTransactionReason{
    #[default]
    NoCommand,
    InvalidCommand,
    MissingParameters{parameters: Vec<String>}
}

#[derive(Debug,PartialEq)]
pub enum Transactions{
    Add{label: String},
    Remove{label: String},
    Rename{from: String, to: String},
    Print,
    Invalid{reason: InvalidTransactionReason}
}

impl Default for Transactions{
    fn default() -> Self {
        Transactions::Invalid { reason: InvalidTransactionReason::default() }
    }
}

impl Transactions{
    pub fn from_parameters(parameters: Vec<String>) -> Transactions{
        let mut parameters = parameters.into_iter();
        if let Some(command) = parameters.next(){
            match command.as_str(){
                "add" => {
                    if let Some(label) = parameters.next(){
                        Transactions::Add { label }
                    }
                    else{
                        Transactions::Invalid { reason: InvalidTransactionReason::MissingParameters { parameters: vec![command] } }
                    }
                },
                "remove" => {
                    if let Some(label) = parameters.next(){
                        Transactions::Remove { label }
                    }
                    else{
                        Transactions::Invalid { reason: InvalidTransactionReason::MissingParameters { parameters: vec![command] } }
                    }
                },
                "rename" => {
                    if let Some(from) = parameters.next(){
                        if let Some(to) = parameters.next(){
                            Transactions::Rename { from, to }
                        }
                        else{
                            Transactions::Invalid { reason: InvalidTransactionReason::MissingParameters { parameters: vec![command,from] } }
                        }
                    }
                    else{
                        Transactions::Invalid { reason: InvalidTransactionReason::MissingParameters { parameters: vec![command] } }
                    }
                }
                "print" => Transactions::Print,
                _ => Transactions::Invalid { reason: InvalidTransactionReason::InvalidCommand }
            }
        }
        else{
            Transactions::Invalid { reason: InvalidTransactionReason::NoCommand }
        }
    }
}