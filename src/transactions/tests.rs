use super::{Transactions,InvalidTransactionReason};
use rstest::rstest;
#[rstest]
#[case(vec![],Transactions::Invalid{reason: InvalidTransactionReason::NoCommand})]
#[case(vec!["add","Take a shower"],Transactions::Add{label: String::from("Take a shower")})]
#[case(vec!["ad","Take a shower"],Transactions::Invalid { reason: InvalidTransactionReason::InvalidCommand })]
#[case(vec!["add"],Transactions::Invalid{reason: InvalidTransactionReason::MissingParameters { parameters: vec!["add".to_string()] }})]
#[case(vec!["remove"],Transactions::Invalid{reason: InvalidTransactionReason::MissingParameters { parameters: vec!["remove".to_string()] }})]
#[case(vec!["remove","Make a date"],Transactions::Remove{label: String::from("Make a date")})]
#[case(vec!["rename"],Transactions::Invalid{reason: InvalidTransactionReason::MissingParameters { parameters: vec!["rename".to_string()] }})]
#[case(vec!["rename","Take a shower"],Transactions::Invalid{reason: InvalidTransactionReason::MissingParameters { parameters: vec!["rename".to_string(),"Take a shower".to_string()] }})]
#[case(vec!["rename","Take a shower","Take a bath"],Transactions::Rename{from: String::from("Take a shower"),to: String::from("Take a bath")})]
#[case(vec!["print"],Transactions::Print)]
fn when_parameters_are_provided_then_transaction_is_constructed(
    #[case] arguments: Vec<&str>,
    #[case] transaction_expected: Transactions
){
    let arguments: Vec<String> = arguments
        .iter()
        .map(|&arg| String::from(arg))
        .collect()
    ;

    let transaction = Transactions::from_parameters(arguments);

    assert_eq!(transaction,transaction_expected);
}