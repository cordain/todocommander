use std::{cmp::Ordering, io::{Read, self, Write}};
use rstest::rstest;
use crate::transactions::{Transactions,InvalidTransactionReason, TransactionResponse};

use super::{ToDoTask, TodoListManager};

#[derive(Default)]
struct FileMock{
    data: Option<String>,
}

impl FileMock{
    fn with_content(content: String) -> FileMock{
        FileMock { data: Some(content) }
    }
}
impl Read for FileMock{
    fn read(&mut self, _: &mut [u8]) -> std::io::Result<usize> {
        Ok(0)//please do not use it :)
    }
    fn read_to_string(&mut self, buf: &mut String) -> io::Result<usize> {
        if let Some(data) = self.data.clone(){
            *buf = data.clone();
            Ok(buf.len())
        }
        else {
            Err(io::Error::new(io::ErrorKind::NotFound, "File does not exist"))
        }
    }
}
impl Write for FileMock{
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        String::from_utf8(buf.to_vec())
            .map_err(|_| io::Error::new(
                io::ErrorKind::InvalidData, 
                "Utf8 error")
            )
            .map(|stringified|{
                self.data = Some(stringified);
                self.data
                .as_ref()
                .unwrap()
                .len()
            })
    }

    fn flush(&mut self) -> io::Result<()> {
        Ok(())//please do not use it :)
    }
}

#[test]
fn when_task_is_spawned_from_label_then_all_its_data_is_default(){
    let label = "Take a shower".to_string();

    let task = ToDoTask::from_label(label.clone());

    assert_eq!(task.get_name(),label);
}

#[test]
fn when_task_is_added_then_it_is_saved_in_runtime(){
    let task = ToDoTask::from_label("Take a shower".to_string());
    let mut manager = TodoListManager::default();

    manager.new_task(task);

    let finding = manager
        .list.iter()
        .find(|task| task.name
            .contains("Take a shower")
        )
    ;
    assert!(finding.is_some());
}

#[rstest]
#[case(vec!["Take a shower"])]
#[case(vec!["task","task2","task3"])]
#[case(vec![])]
fn when_task_list_needs_to_be_read_from_runtime_then_task_list_is_returned(
    #[case] input_list: Vec<&str>
){
    let input_expected: Vec<ToDoTask> = input_list
        .into_iter()
        .map(|item| ToDoTask::from_label(item.to_owned()))
        .collect()
    ;
    let manager = {
        let mut manager = TodoListManager::default();
        manager.list.resize(input_expected.len(), ToDoTask::from_label(String::default()));
        manager.list.clone_from_slice(input_expected.as_slice());
        manager
    };

    let read_list = manager.get_tasks();

    assert_eq!(read_list.iter().cmp(input_expected.iter()),Ordering::Equal);

}
#[rstest]
#[case(vec!["Take a shower"])]
#[case(vec!["task","task2","task3"])]
#[case(vec![])]
fn given_file_exists_when_manager_opens_file_then_content_is_loaded(
    #[case] input_list: Vec<&str>
){
    let mut manager = TodoListManager::default();
    let file_content = input_list.join("\n");
    let mut reader = FileMock::with_content(file_content);

    let loaded_data = manager.try_read(&mut reader);

    let list_expected: Vec<String> = input_list.iter().map(|label| String::from(*label)).collect();
    assert_eq!(list_expected.iter().cmp(loaded_data.unwrap().iter()), Ordering::Equal);
}
#[test]
fn given_file_does_not_exists_when_manager_opens_file_then_error_is_returned(){
    let mut manager = TodoListManager::default();
    let mut reader = FileMock::default();

    let loaded_data = manager.try_read(&mut reader);

    assert_eq!(loaded_data.err().unwrap().kind(),io::ErrorKind::NotFound);
}

#[rstest]
#[case(vec!["Take a shower"])]
#[case(vec!["task","task2","task3"])]
#[case(vec![])]
fn given_file_exists_when_file_is_not_empty_then_list_is_loaded_with_data_in_file(
    #[case] input_list: Vec<&str>
){
    let file_content = input_list.join("\n");
    let mut reader = FileMock::with_content(file_content);

    let manager = TodoListManager::from_file(&mut reader);

    let list_expected: Vec<ToDoTask> = input_list
        .iter()
        .map(|label| ToDoTask::from_label(String::from(*label)))
        .collect()
    ;
    //sanity check
    assert_eq!(list_expected.iter().cmp(manager.unwrap().list.iter()), Ordering::Equal);
}
#[test]
fn given_file_exists_when_file_is_empty_then_empty_list_is_initialized(){
    let mut reader = FileMock::default();

    let manager = TodoListManager::from_file(&mut reader);

    //sanity check
    assert_eq!(vec![].iter().cmp(manager.unwrap().list.iter()),Ordering::Equal);
}
#[rstest]
#[case(vec!["Take a shower"])]
#[case(vec!["task","task2","task3"])]
#[case(vec![])]
fn when_manager_closes_then_list_is_saved_to_a_file(
    #[case] input_list: Vec<&str>
){
    let mut writer = FileMock::with_content(String::default());
    let mut manager = TodoListManager::default();
    let mut list_converted: Vec<ToDoTask> = input_list.iter().map(|&item| ToDoTask::from_label(String::from(item))).collect();
    manager.list.append(&mut list_converted);

    manager.close(&mut writer).unwrap();

    let file_expected = input_list.join("\n");
    assert!(file_expected.contains(&writer.data.unwrap()));
}

#[test]
fn given_list_has_tasks_when_not_present_task_is_deleted_then_error_is_returned(){
    let mut manager = TodoListManager::default();
    manager.new_task(ToDoTask::from_label(String::from("Do homework")));

    let result = manager.delete_by_name(String::from("Take a shower"));

    assert_eq!(result,Err("Removing task that does not exist"));
}
#[test]
fn given_list_is_empty_when_task_is_deleted_then_error_is_returned(){
    let mut manager = TodoListManager::default();

    let result = manager.delete_by_name(String::from("Take a shower"));

    assert_eq!(result,Err("Removing from empty list"));
}
#[rstest]
#[case(vec!["Take a shower"],"Take a shower",vec![])]
#[case(vec!["Take a shower","Do homework"],"Do homework",vec!["Take a shower"])]
fn given_list_has_following_tasks_when_task_is_deleted_then_task_is_no_longer_present_in_task_list(
    #[case] input_list: Vec<&str>,
    #[case] taskname: &str,
    #[case] output_list: Vec<&str>
){
    let mut manager = TodoListManager::default();
    for &task in input_list.iter(){
        manager.new_task(ToDoTask::from_label(String::from(task)));
    }

    manager.delete_by_name(String::from(taskname)).unwrap();

    let output_list_converted: Vec<ToDoTask> = output_list
        .iter()
        .map(|&item| ToDoTask::from_label(String::from(item)))
        .collect()
    ;
    assert_eq!(output_list_converted.iter().cmp(manager.list.iter()),Ordering::Equal);
}
#[test]
fn given_list_has_tasks_when_not_present_task_is_renamed_then_error_is_returned(){
    let mut manager = TodoListManager::default();
    manager.new_task(ToDoTask::from_label(String::from("Do homework")));

    let result = manager.rename_task(
        String::from("Take a shower"),
        String::from("Take a bath")
    );

    assert_eq!(result,Err("Renaming task that does not exist"));
}
#[test]
fn given_list_is_empty_when_task_is_renamed_then_error_is_returned(){
    let mut manager = TodoListManager::default();

    let result = manager.rename_task(
        String::from("Take a shower"),
        String::from("Take a bath")
    );

    assert_eq!(result,Err("Renaming task that does not exist"));
}
#[rstest]
#[case(vec!["Take a shower"],"Take a shower","Take a bath",vec!["Take a bath"])]
#[case(vec!["Take a shower","Do homework"],"Do homework","Play with toys",vec!["Take a shower","Play with toys"])]
fn given_list_has_following_items_when_task_is_renamed_then_task_resumes_under_different_name(
    #[case] input_list: Vec<&str>,
    #[case] taskname_from: &str,
    #[case] taskname_to: &str,
    #[case] output_list: Vec<&str>
){
    let mut manager = TodoListManager::default();
    for &task in input_list.iter(){
        manager.new_task(ToDoTask::from_label(String::from(task)));
    }

    manager.rename_task(
        String::from(taskname_from),
        String::from(taskname_to)
    ).unwrap();

    let output_list_converted: Vec<ToDoTask> = output_list
        .iter()
        .map(|&item| ToDoTask::from_label(String::from(item)))
        .collect()
    ;
    assert_eq!(output_list_converted.iter().cmp(manager.list.iter()),Ordering::Equal);
}
#[rstest]
#[case(Transactions::Add{label: String::from("Take a shower")},vec!["Go to shop","Make a date","Take a shower"])]
#[case(Transactions::Remove{label: String::from("Make a date")},vec!["Go to shop"])]
#[case(Transactions::Rename{from: String::from("Go to shop"),to: String::from("Go to bar")},vec!["Go to bar","Make a date"])]
#[case(Transactions::Print,vec!["Go to shop","Make a date"])]
fn given_assumed_task_list_when_valid_transaction_is_passed_then_adequate_action_is_provided(
    #[case] transaction: Transactions,
    #[case] list_expected: Vec<&str>
){
    let mut manager = TodoListManager::default();
    manager.new_task(ToDoTask::from_label(String::from("Go to shop")));
    manager.new_task(ToDoTask::from_label(String::from("Make a date")));

    manager.execute(transaction).unwrap();

    let list_expected: Vec<ToDoTask> = list_expected
        .iter()
        .map(|&label| ToDoTask::from_label(String::from(label)))
        .collect()
    ;
    assert_eq!(list_expected.iter().cmp(manager.list.iter()),Ordering::Equal);
}

#[rstest]
#[case(Transactions::Invalid{reason: InvalidTransactionReason::NoCommand},Err("No command provided"))]
#[case(Transactions::Invalid{reason: InvalidTransactionReason::InvalidCommand},Err("Command not found"))]
#[case(Transactions::Invalid{reason: InvalidTransactionReason::MissingParameters { parameters: vec!["add".to_string()] }},Err("No parameters provided for add command"))]
#[case(Transactions::Invalid{reason: InvalidTransactionReason::MissingParameters { parameters: vec!["remove".to_string()] }},Err("No label provided to remove command"))]
#[case(Transactions::Invalid{reason: InvalidTransactionReason::MissingParameters { parameters: vec!["rename".to_string()] }},Err("No label provided to rename command"))]
#[case(Transactions::Invalid{reason: InvalidTransactionReason::MissingParameters { parameters: vec!["rename".to_string(), "Take a shower".to_string()] }},Err("Renaming task that does not exist"))]
fn when_invalid_transaction_is_passed_then_error_is_provided(
    #[case] transaction: Transactions,
    #[case] execute_result_expected: Result<TransactionResponse,&str>
){
    let mut manager = TodoListManager::default();

    let execute_result = manager.execute(transaction);

    assert_eq!(execute_result,execute_result_expected);
}

#[test]
fn given_renamed_task_is_presend_when_invalid_rename_transaction_is_passed_then_error_is_provided(){
    let mut manager = TodoListManager::default();
    manager.new_task(ToDoTask::from_label("Take a shower".to_string()));
    let transaction = Transactions::Invalid { reason: InvalidTransactionReason::MissingParameters {parameters: vec!["rename".to_string(),"Take a shower".to_string()]} };
    
    let execute_result = manager.execute(transaction);

    assert_eq!(execute_result,Err("Task found, but new task name not provided"));

}

#[rstest]
#[case(Transactions::Add{label: "Take a shower".to_string()},Err("Adding task that already exists"))]
#[case(Transactions::Remove{label: "Take a bath".to_string()},Err("Removing task that does not exist"))]
#[case(Transactions::Rename{from: "Take a bath".to_string(), to: "Take out trash".to_string()},Err("Renaming task that does not exist"))]
fn given_task_list_contains_assumed_items_when_transaction_with_invalid_data_is_passed_then_error_is_provided(
    #[case] transaction: Transactions,
    #[case] execute_result_expected: Result<TransactionResponse,&str>
){
    let mut manager = TodoListManager::default();
    manager.new_task(ToDoTask::from_label("Take a shower".to_string()));

    let execute_result = manager.execute(transaction);

    assert_eq!(execute_result,execute_result_expected);
}

#[test]
fn given_task_list_contains_assumed_items_when_print_transaction_is_received_then_actual_list_is_returned_as_response(){
    let mut manager = TodoListManager::default();
    manager.new_task(ToDoTask::from_label(String::from("Go to shop")));
    manager.new_task(ToDoTask::from_label(String::from("Make a date")));
    let transaction = Transactions::Print;

    let response = manager.execute(transaction).unwrap();

    let list_expected = vec![
        "Go to shop".to_string(),
        "Make a date".to_string()
    ];
    if let TransactionResponse::PrintList { list } = response {
        assert_eq!(list_expected.iter().cmp(list.iter()),Ordering::Equal);
    }
    else{
        panic!()
    }
}