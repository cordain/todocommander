use crate::transactions::{Transactions, InvalidTransactionReason, TransactionResponse};

#[cfg(test)]
mod tests;

#[derive(Debug,Clone,Ord,PartialEq, PartialOrd,Eq)]
pub struct ToDoTask{
    name: String
}
#[derive(Default,Debug)]
pub struct TodoListManager{
    list: Vec<ToDoTask>,
}

impl ToDoTask{
    pub fn from_label(name: String) -> ToDoTask{
        ToDoTask { name }
    }
    pub fn get_name(&self) -> String{
        self.name.clone()
    }
    pub fn rename(&mut self,newname: String){
        self.name = newname;
    }
}

impl TodoListManager{
    pub fn from_file(file: &mut impl std::io::Read) -> Option<TodoListManager>{
        let mut manager = TodoListManager{ list: vec![] };
        manager
            .try_read(file)
            .unwrap_or_default()
            .iter()
            .for_each(|task|{
                manager.new_task(ToDoTask::from_label(task.to_owned()));
            })
        ;
        Some(manager)
    }

    fn get_rename_error(&self, task_param: Option<String>) -> Result<TransactionResponse,&str>{
        if let Some(task) = task_param{
            if let None = self.find_task(task){
                Err("Renaming task that does not exist")
            }
            else{
                Err("Task found, but new task name not provided")
            }
        }
        else {
            Err("No label provided to rename command")
        }

    }

    fn match_invalid_transaction_reason(&self, reason: InvalidTransactionReason) -> Result<TransactionResponse,&str>{
        match reason {
            InvalidTransactionReason::NoCommand => Err("No command provided"),
            InvalidTransactionReason::InvalidCommand => Err("Command not found"),
            InvalidTransactionReason::MissingParameters { parameters } => {
                let mut parameters = parameters.into_iter();
                match parameters.next().unwrap().as_str(){
                    "add" => Err("No parameters provided for add command"),
                    "remove" => Err("No label provided to remove command"),
                    "rename" => {
                        self.get_rename_error(parameters.next())
                    },
                    _ => unreachable!()
                }
            }
        }
    }

    pub fn execute(&mut self, transaction: Transactions) -> Result<TransactionResponse,&str>{
        match transaction {
            Transactions::Add { label } => {
                if self.find_task(label.clone()).is_none(){
                    self.new_task(ToDoTask::from_label(String::from(label)));
                    Ok(TransactionResponse::Empty)
                }
                else{
                    Err("Adding task that already exists")
                }
            },
            Transactions::Remove { label } => {
                self.delete_by_name(String::from(label)).map(|_| TransactionResponse::Empty)
            },
            Transactions::Rename { from, to } => {
                self.rename_task(String::from(from), String::from(to)).map(|_| TransactionResponse::Empty)
            },
            Transactions::Invalid { reason } => {
                self.match_invalid_transaction_reason(reason)
            }
            Transactions::Print => Ok(TransactionResponse::PrintList { list: self.list
                .clone()
                .iter()
                .map(|item| item.get_name())
                .collect() 
            })
        }
    }

    pub fn new_task(&mut self, task: ToDoTask){
        self.list.push(task);
    }

    pub fn delete_by_name(&mut self, taskname: String) -> Result<(),&str>{
        if self.list.is_empty(){
            Err("Removing from empty list")
        }
        else{
            let index = self
                .find_task(taskname)
                .ok_or("Removing task that does not exist")
            ?;
            self.list.swap_remove(index);
            Ok(())
        }
    }

    pub fn get_tasks(&self) -> Vec<ToDoTask>{
        self.list.clone()
    }
    
    pub fn rename_task(&mut self, taskname: String, new_taskname: String) -> Result<(),&str>{
        let index = self
            .find_task(taskname)
            .ok_or("Renaming task that does not exist")
        ?;
        self.list[index].rename(new_taskname);
        Ok(())
    }

    pub fn close(&mut self, writer: &mut impl std::io::Write) -> std::io::Result<()>{
        let stringlist: Vec<String> = self.list
            .iter()
            .map(|task| task.get_name())
            .clone()
            .collect()
        ;
        writer.write(stringlist.join("\n").as_bytes()).map(|_|())
    }

    fn find_task(&self, name: String) -> Option<usize>{
        self.list
            .iter()
            .position(|task| task
                .get_name()
                .contains(&name)
            )   
    }

    fn try_read(&mut self, reader: &mut impl std::io::Read) -> std::io::Result<Vec<String>>{
        let mut buffer = String::default();
        reader.read_to_string(&mut buffer)?;
        let list: Vec<String> = if buffer.is_empty(){
            vec![]
        }
        else{
            buffer.split("\n").map(|label| String::from(label)).collect()
        };

        Ok(list)
    }

}
